mod html;
mod model;

use std::io;

use anyhow::Context;
use automerge::{
    sync::{self, SyncDoc},
    Automerge,
};

use autosurgeon::{hydrate, reconcile};

pub use html::find_rss_links;
pub use model::{Channel, Item, Model, PubDate};

#[derive(Debug)]
pub struct Db {
    model: Model,
    crdt: Automerge,
}

impl Default for Db {
    fn default() -> Self {
        let (model, crdt) = Model::schema().unwrap();
        Self { model, crdt }
    }
}

impl Db {
    pub fn load(mut file: impl io::Read) -> anyhow::Result<Self> {
        let mut buf = Vec::new();
        file.read_to_end(&mut buf)
            .context("failed to read serialized CRDT")?;
        let crdt = Automerge::load(&buf).context("failed to parse serialized CRDT")?;
        let model = hydrate(&crdt).context("failed to hydrate model")?;
        let mut this = Self { model, crdt };
        this.enforce_invariants();
        Ok(this)
    }

    pub fn save(&self, mut file: impl io::Write) -> anyhow::Result<()> {
        let buf = self.crdt.save();
        file.write_all(buf.as_slice())
            .context("failed to write serialized CRDT")?;
        Ok(())
    }

    pub fn model(&self) -> &Model {
        &self.model
    }

    pub fn crdt(&self) -> &Automerge {
        &self.crdt
    }

    pub fn generate_sync_message(&mut self, state: &mut sync::State) -> Option<sync::Message> {
        self.crdt.generate_sync_message(state)
    }

    pub fn receive_sync_message(
        &mut self,
        state: &mut sync::State,
        message: sync::Message,
    ) -> anyhow::Result<()> {
        self.crdt.receive_sync_message(state, message)?;
        self.model = hydrate(&self.crdt).context("failed to hydrate model")?;
        self.enforce_invariants();
        Ok(())
    }

    pub fn try_update<F, O, E>(&mut self, f: F) -> Result<O, E>
    where
        F: FnOnce(&mut Model) -> Result<O, E>,
        E: From<autosurgeon::ReconcileError>,
    {
        let ret = f(&mut self.model)?;
        self.enforce_invariants();
        match self.crdt.transact(|doc| reconcile(doc, &self.model)) {
            Ok(_) => Ok(ret),
            Err(automerge::transaction::Failure { error, .. }) => Err(error.into()),
        }
    }

    pub fn update<F, O>(&mut self, f: F) -> Result<O, autosurgeon::ReconcileError>
    where
        F: FnOnce(&mut Model) -> O,
    {
        self.try_update(|model| Ok(f(model)))
    }

    fn enforce_invariants(&mut self) {
        // Enforce that every item must have a corresponding channel
        self.model
            .items
            .retain(|_, Item { channel_id, .. }| self.model.channels.contains_key(channel_id));
    }
}

pub fn parse_feed(
    feed_url: &str,
    feed_body: &[u8],
) -> anyhow::Result<(String, Channel, Vec<(String, Item)>)> {
    use atom_syndication as atom;
    use time::format_description::well_known::{Rfc2822, Rfc3339};

    if let Ok(atom::Feed {
        id: mut channel_id,
        title,
        mut links,
        entries,
        ..
    }) = atom::Feed::read_from(&feed_body[..])
    {
        if channel_id.is_empty() {
            // Even though `id` is a required atom field, sometimes it's
            // missing. In that case, just use the feed URL.
            channel_id = feed_url.to_owned();
        }
        // [rel="self"] is the URL of the Atom feed (i.e., `feed_url`). So, we
        // don't wanna take that, unless we have nothing else.
        let not_rel_self_index = links.iter().position(|link| link.rel != "self");
        let link = if let Some(index) = not_rel_self_index {
            // Take the first not-rel-"self" link
            Some(links.remove(index).href)
        } else {
            // Otherwise, take the first link
            links.into_iter().next().map(|link| link.href)
        };

        let channel = Channel {
            feed_url: feed_url.to_owned(),
            title: Some(title.value),
            link,
        };

        let items: Vec<_> = entries
            .into_iter()
            .map(
                |atom::Entry {
                     title,
                     id: mut item_id,
                     links,
                     updated,
                     published,
                     ..
                 }| {
                    let title = title.value;
                    let pub_date = published.unwrap_or(updated);
                    let pub_date = PubDate::from_timestamp(pub_date.timestamp());
                    let link = links.into_iter().next().map(|link| link.href);
                    if item_id.is_empty() {
                        // Same as above, sometimes there's no ID.
                        item_id = link.clone().unwrap_or_else(|| title.clone());
                        debug_assert!(!item_id.is_empty(), "empty ID for item");
                    }
                    (
                        item_id,
                        Item {
                            channel_id: channel_id.clone(),
                            title: Some(title),
                            link,
                            pub_date,
                            read: false,
                        },
                    )
                },
            )
            .collect();

        Ok((channel_id, channel, items))
    } else if let Ok(rss::Channel {
        title, link, items, ..
    }) = rss::Channel::read_from(&feed_body[..])
    {
        let channel_id = feed_url.to_owned();
        let channel = Channel {
            feed_url: feed_url.to_owned(),
            title: Some(title),
            link: Some(link),
        };

        // The channel's items
        let items: Vec<_> = items
            .into_iter()
            .flat_map(
                |rss::Item {
                     guid,
                     title,
                     link,
                     pub_date,
                     ..
                 }| {
                    // We skip missing IDs, since we can't track them.
                    let item_id = guid.map(|guid| guid.value).or_else(|| link.clone())?;
                    let pub_date = pub_date
                        .map(|s| PubDate::from_rfc2822(&s))
                        .unwrap_or_default();
                    Some(Ok((
                        item_id,
                        Item {
                            channel_id: channel_id.clone(),
                            title,
                            link,
                            pub_date,
                            read: false,
                        },
                    )))
                },
            )
            .collect::<anyhow::Result<_>>()?;

        Ok((channel_id, channel, items))
    } else {
        anyhow::bail!("failed to parse RSS/Atom feed");
    }
}
