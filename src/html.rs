/// List all RSS and atom links in an HTML document, returning their `href`
/// attribute.
pub fn find_rss_links(mut doc: &[u8]) -> Vec<&str> {
    let mut links = vec![];
    while !doc.is_empty() {
        let Some(index) = find(doc, b"<link") else {
            break;
        };
        doc = &doc[index..];
        let Some(end_index) = find(doc, b">") else {
            break;
        };
        let inner = &doc[..end_index];
        doc = &doc[end_index + 1..];

        let Some(type_index) = find(inner, b"type=\"") else {
            continue;
        };
        let type_ = &inner[type_index + 6..];
        let Some(end_index) = find(type_, b"\"") else {
            continue;
        };
        let type_ = trim(&type_[..end_index]);
        if type_ != b"application/rss+xml" && type_ != b"application/atom+xml" {
            continue;
        }

        let Some(href_index) = find(inner, b"href=\"") else {
            continue;
        };
        let href = &inner[href_index + 6..];
        let Some(end_index) = find(href, b"\"") else {
            continue;
        };
        let href = &href[..end_index];
        if let Ok(href) = std::str::from_utf8(href) {
            links.push(href);
        }
    }
    links
}

fn find(buf: &[u8], pat: &[u8]) -> Option<usize> {
    for i in 0..(buf.len() - pat.len() + 1) {
        if &buf[i..(i + pat.len())] == pat {
            return Some(i);
        }
    }
    None
}

fn trim(mut buf: &[u8]) -> &[u8] {
    for i in 0..buf.len() {
        if !buf[i].is_ascii_whitespace() {
            buf = &buf[i..];
            break;
        }
    }
    for i in (0..buf.len()).rev() {
        if !buf[i].is_ascii_whitespace() {
            buf = &buf[..=i];
            break;
        }
    }
    buf
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_find_rss_links() {
        let text = include_bytes!("../tests/assets/wiki.html");
        let links = find_rss_links(text);
        assert_eq!(
            &links,
            &[
                "/w/api.php?action=featuredfeed&amp;feed=potd&amp;feedformat=atom",
                "/w/api.php?action=featuredfeed&amp;feed=featured&amp;feedformat=atom",
                "/w/api.php?action=featuredfeed&amp;feed=onthisday&amp;feedformat=atom",
                "/w/index.php?title=Special:RecentChanges&amp;feed=atom",
            ]
        );
    }
}
