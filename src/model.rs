#![allow(non_camel_case_types)]

use std::collections::BTreeMap;

use anyhow::{Context, Result};
use autosurgeon::{Hydrate, Reconcile};
use time::OffsetDateTime;

#[derive(Debug, Clone, Default, Hydrate, Reconcile)]
pub struct Model {
    pub channels: BTreeMap<String, Channel>,
    pub items: BTreeMap<String, Item>,
}

#[derive(Debug, Clone, Hydrate, Reconcile)]
pub struct Channel {
    pub feed_url: String,
    pub title: Option<String>,
    pub link: Option<String>,
}

impl Channel {
    pub fn new(feed_url: String) -> Self {
        Self {
            feed_url,
            title: None,
            link: None,
        }
    }
}

#[derive(Debug, Clone, Hydrate, Reconcile)]
pub struct Item {
    pub channel_id: String,
    pub title: Option<String>,
    pub link: Option<String>,
    // #[autosurgeon(with = "autosurgeon_option_offset_date_time")]
    pub pub_date: PubDate,
    pub read: bool,
}

impl Item {
    pub fn new(channel_id: String) -> Self {
        Self {
            channel_id,
            title: None,
            link: None,
            pub_date: PubDate::default(),
            read: false,
        }
    }
}

impl Model {
    pub fn schema() -> Result<(Self, automerge::Automerge)> {
        // FIXME long-term it's probably more stable to store the actual bytes
        // in a file. That way, any involuntary change to the schema would be
        // reflected in a hydration error.
        let model = Model::default();
        let mut doc =
            automerge::Automerge::new().with_actor(automerge::ActorId::from(b"TRASS_SCHEMA"));
        doc.transact_with(
            |_| automerge::transaction::CommitOptions {
                message: Some("TRASS SCHEMA".to_owned()),
                time: Some(0),
            },
            |t| autosurgeon::reconcile(t, &model).context("could not reconcile schema"),
        )
        .map_err(|e| e.error)?;
        doc.set_actor(automerge::ActorId::random());
        Ok((model, doc))
    }

    pub fn hydrate_channel(&mut self, feed_url: &str, feed_body: &[u8]) -> anyhow::Result<()> {
        let (channel_id, channel, items) = crate::parse_feed(feed_url, feed_body)?;
        assert!(
            !channel_id.is_empty(),
            "empty channel ID for channel `{channel:?}`"
        );

        for (item_id, item) in items {
            assert!(!item_id.is_empty(), "empty item ID for item `{item:?}`");
            let read = self.items.get(&item_id).map_or(false, |item| item.read);
            self.items.insert(item_id, Item { read, ..item });
        }

        self.channels.insert(channel_id.clone(), channel);

        Ok(())
    }
}

#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub enum PubDate {
    Parsed(OffsetDateTime),
    Raw(String),
    #[default]
    Missing,
}

impl PubDate {
    pub fn from_rfc3339(s: impl AsRef<str>) -> Self {
        use time::format_description::well_known::Rfc3339;
        OffsetDateTime::parse(s.as_ref(), &Rfc3339).map_or_else(
            |_| PubDate::Raw(s.as_ref().to_owned()),
            |dt| PubDate::Parsed(dt),
        )
    }

    pub fn from_rfc2822(s: impl AsRef<str>) -> Self {
        use time::format_description::well_known::Rfc2822;
        OffsetDateTime::parse(s.as_ref(), &Rfc2822).map_or_else(
            |_| PubDate::Raw(s.as_ref().to_owned()),
            |dt| PubDate::Parsed(dt),
        )
    }

    pub fn from_timestamp(t: i64) -> Self {
        OffsetDateTime::from_unix_timestamp(t)
            .map(|dt| PubDate::Parsed(dt))
            .unwrap_or_default()
    }
}

impl Hydrate for PubDate {
    fn hydrate_int(i: i64) -> std::result::Result<Self, autosurgeon::HydrateError> {
        Hydrate::hydrate_timestamp(i)
    }

    fn hydrate_uint(u: u64) -> std::result::Result<Self, autosurgeon::HydrateError> {
        Hydrate::hydrate_timestamp(u as _)
    }

    fn hydrate_string(string: &'_ str) -> std::result::Result<Self, autosurgeon::HydrateError> {
        Ok(PubDate::Raw(string.to_owned()))
    }

    fn hydrate_timestamp(t: i64) -> std::result::Result<Self, autosurgeon::HydrateError> {
        OffsetDateTime::from_unix_timestamp(t)
            .map(PubDate::Parsed)
            .map_err(|err| {
                autosurgeon::HydrateError::unexpected("a valid unix timestamp", err.to_string())
            })
    }

    fn hydrate_none() -> Result<Self, autosurgeon::HydrateError> {
        Ok(PubDate::Missing)
    }
}

impl Reconcile for PubDate {
    type Key<'a> = autosurgeon::reconcile::NoKey;

    fn reconcile<R: autosurgeon::Reconciler>(
        &self,
        mut reconciler: R,
    ) -> std::result::Result<(), R::Error> {
        match self {
            PubDate::Parsed(dt) => reconciler.timestamp(dt.unix_timestamp()),
            PubDate::Raw(s) => reconciler.str(s),
            PubDate::Missing => reconciler.none(),
        }
    }
}

mod autosurgeon_option_offset_date_time {
    use autosurgeon::{Hydrate, HydrateError, Prop, ReadDoc, Reconciler};
    use time::OffsetDateTime;

    const DATETIME_FORMAT: time::format_description::well_known::Rfc3339 =
        time::format_description::well_known::Rfc3339;

    pub(super) fn hydrate<'a, D: ReadDoc>(
        doc: &D,
        obj: &automerge::ObjId,
        prop: Prop<'a>,
    ) -> Result<Option<OffsetDateTime>, HydrateError> {
        let inner = Option::<String>::hydrate(doc, obj, prop)?;
        inner
            .map(|inner| {
                OffsetDateTime::parse(&inner, &DATETIME_FORMAT).map_err(|e| {
                    HydrateError::unexpected(
                        "a valid ISO-8601 datetime",
                        format!("a datetime which failed to parse due to {}", e),
                    )
                })
            })
            .transpose()
    }

    pub(super) fn reconcile<R: Reconciler>(
        dt: &Option<OffsetDateTime>,
        mut reconciler: R,
    ) -> Result<(), R::Error> {
        if let Some(dt) = dt {
            let inner = dt.format(&DATETIME_FORMAT).unwrap();
            reconciler.str(inner)
        } else {
            reconciler.none()
        }
    }
}
