use std::{
    collections::{btree_map::Entry, BTreeMap},
    path::PathBuf,
    time::Duration,
};

use anyhow::{anyhow, bail, Context};
use automerge::sync::SyncDoc;
use autosurgeon::{hydrate, reconcile};
use clap::{Parser, Subcommand};
use futures::StreamExt;
use time::OffsetDateTime;
use trass::{Channel, Db, Item, Model};
use url::Url;
use uuid::Uuid;

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    let args = Cli::parse();

    let doc_path = std::path::Path::new("trass.automerge");
    let mut db = match std::fs::File::open(doc_path) {
        Ok(file) => Db::load(file).context("failed to load CRDT from file")?,
        Err(e) if e.kind() == std::io::ErrorKind::NotFound => Db::default(),
        Err(e) => bail!("failed to read file: {e}"),
    };

    let mut changed = false;
    match args.command.unwrap_or_default() {
        Command::List => {
            if db.model().channels.is_empty() {
                println!("Not following any channels.")
            } else {
                for (
                    i,
                    (
                        channel_id,
                        Channel {
                            feed_url,
                            title,
                            link,
                        },
                    ),
                ) in db.model().channels.iter().enumerate()
                {
                    println!("\n")
                    // let n_unread = items.values().filter(|item| !item.read).count();
                    // println!(
                    //     "\n{}) {url}\n\t{n_unread} unread\n\t{}",
                    //     i + 1,
                    //     if let Some(last_check) = last_check {
                    //         format!("last pulled on {last_check}")
                    //     } else {
                    //         format!("never pulled")
                    //     }
                    // );
                }
            }
        }
        Command::Show { pattern } => {
            todo!()
            // let channel_id = channel_id_for_pattern(db.model(), pattern)?;
            // let items = db
            //     .model()
            //     .items
            //     .iter()
            //     .filter(|(_, item)| item.channel_id == channel_id);
            // let channel = db.model().channels.get(channel_id).unwrap();
            // if channel.items.is_empty() {
            //     println!("No items found locally for channel '{url}'. Try `pull`ing for updates.");
            // } else {
            //     println!("Local items for channel '{url}':");
            //     let (read, unread) = channel
            //         .items
            //         .iter()
            //         .enumerate()
            //         .partition::<Vec<_>, _>(|(_, (_, item))| item.read);
            //     if read.is_empty() {
            //         println!("\nNo read items.")
            //     } else {
            //         println!("\nRead:");
            //         for (i, (url, _)) in read {
            //             println!("{}) {url}", i + 1);
            //         }
            //     }
            //     if unread.is_empty() {
            //         println!("\nNo unread items.")
            //     } else {
            //         println!("\nUnread:");
            //         for (i, (url, _)) in unread {
            //             println!("{}) {url}", i + 1);
            //         }
            //     }
            // }
        }
        Command::Pull => {
            todo!()
            // println!("Pulling for new items...");
            // let urls = trass.channels.keys().cloned().collect::<Vec<_>>();
            // let mut items = futures::stream::iter(urls.into_iter().map(|url| pull_channel(url)))
            //     .buffer_unordered(8);
            // while let Some(res) = items.next().await {
            //     // TODO report errors
            //     let Ok((url, items)) = res else { continue };
            //     let channel = trass.channels.get_mut(&url).unwrap();
            //     let mut new = 0;
            //     for url in items {
            //         channel.items.entry(url).or_insert_with(|| {
            //             new += 1;
            //             Item { read: false }
            //         });
            //     }
            //     if new > 0 {
            //         println!(
            //             "Found {new} new item{} for '{url}'.",
            //             if new == 1 { "" } else { "s" }
            //         );
            //     }
            //     channel.last_check = Some(OffsetDateTime::now_utc());
            //     changed = true;
            // }
            // if !changed {
            //     println!("Already up to date.")
            // }
        }
        Command::Mark {
            pattern,
            index,
            unread,
        } => {
            todo!();
            // let url = channel_id_for_pattern(&trass, pattern)?.clone();
            // let channel = trass.channels.get_mut(&url).unwrap();
            // let (url, item) = channel
            //     .items
            //     .iter_mut()
            //     .nth(index)
            //     .context("Index out of bounds")?;
            // if item.read != !unread {
            //     item.read = !unread;
            //     println!(
            //         "Item '{url}' marked as {}.",
            //         if unread { "unread" } else { "read" },
            //     );
            //     changed = true;
            // } else {
            //     println!("Nothing to do.");
            // }
        }
        Command::Add { feed_url } => {
            if db.model().channels.contains_key(&feed_url) {
                let body = reqwest::get(&feed_url)
                    .await
                    .with_context(|| format!("could not fetch URL '{feed_url}'"))?
                    .bytes()
                    .await
                    .with_context(|| format!("could not read body of URL '{feed_url}'"))?;
                db.update(|model| model.hydrate_channel(&feed_url, &body))
                    .context("failed to add channel to CRDT")?;
                changed = true;
                println!("Added channel '{feed_url}'.");
            } else {
                bail!("Channel '{feed_url}' already exists.");
            }
        }
        Command::Remove { pattern } => {
            let channel_id = channel_id_for_pattern(db.model(), pattern)?.to_owned();
            db.update(|model| model.channels.remove(&channel_id));
            changed = true;
            println!("Removed channel '{}'.", channel_id);
        }
        Command::Edit => {
            todo!()
            // use std::io::{Read, Seek, Write};

            // let editor =
            //     std::env::var("EDITOR").context("no editor, set environment variable `EDITOR`")?;

            // let (mut file, path) = tempfile::NamedTempFile::new()
            //     .context("could not create a temporary file")?
            //     .into_parts();

            // for (url, channel) in &db.model().channels {
            //     write!(file, "# {url}\n\n").context("could not write to file")?;
            //     for (url, item) in &channel.items {
            //         write!(file, "- [{}] {url}\n", if item.read { "x" } else { " " })
            //             .context("could not write to file")?;
            //     }
            //     write!(file, "\n\n").context("could not write to file")?;
            // }

            // std::process::Command::new(editor)
            //     .arg(path.to_path_buf())
            //     .status()
            //     .context("could not run editor")?;

            // let mut buf = String::new();
            // file.seek(std::io::SeekFrom::Start(0))
            //     .context("could not seek from start")?;
            // file.read_to_string(&mut buf)
            //     .context("could not read file")?;

            // let edits = parse_edit(&buf)
            //     .map_err(|err| anyhow!(err.to_string()))
            //     .context("could not parse file")?;

            // let mut retained = BTreeMap::new();
            // for (url, items) in edits {
            //     let channel;
            //     if let Some(mut chan) = trass.channels.remove(&url) {
            //         for (url, item) in items {
            //             if let Some(it) = chan.items.get_mut(&url) {
            //                 it.read = item.read;
            //             }
            //         }
            //         channel = chan;
            //     } else {
            //         channel = Channel::default();
            //     }
            //     retained.insert(url, channel);
            // }
            // trass.channels = retained;
            // changed = true;
        }
        Command::Sync => {
            println!("Synchronizing with server...");

            let mut state = automerge::sync::State::new();
            let client = reqwest::Client::builder()
                .build()
                .context("failed to build reqwest client")?;

            let mut sync_message = db.generate_sync_message(&mut state);
            loop {
                let resp = client
                    .post(args.server_url.join("sync").context("invalid server URL")?)
                    .body(
                        autostore_protocol::encode(
                            args.document_id,
                            db.crdt().get_actor(),
                            sync_message,
                        )
                        .context("could not encode sync message")?,
                    )
                    .send()
                    .await
                    .context("failed to send request")?
                    .error_for_status()
                    .context("received HTTP error")?
                    .bytes()
                    .await
                    .context("could not read response body")?;
                let (document_id_resp, _, incoming_message) =
                    autostore_protocol::decode(resp).context("could not decode sync message")?;

                if document_id_resp != args.document_id {
                    bail!("received updates for a different document");
                }
                let incoming_is_none = incoming_message.is_none();
                if let Some(sync_message) = incoming_message {
                    db.receive_sync_message(&mut state, sync_message)
                        .context("could not apply sync message")?;
                }

                sync_message = db.generate_sync_message(&mut state);
                if incoming_is_none && sync_message.is_none() {
                    break;
                }

                print!(".");
            }
            changed = true;
            println!("Done.")
        }
        Command::Import { path } => {
            fn flatten_outlines(mut outlines: Vec<opml::Outline>) -> Vec<opml::Outline> {
                let mut i = 0;
                loop {
                    if i == outlines.len() {
                        break;
                    }
                    let mut nested = std::mem::take(&mut outlines[i].outlines);
                    outlines.append(&mut nested);
                    i += 1;
                }
                outlines
            }
            let buf = std::fs::read_to_string(path).context("could not read OPML file")?;
            let document = opml::OPML::from_str(&buf).context("could not parse OPML file")?;
            let outlines = flatten_outlines(document.body.outlines);
            let client = reqwest::Client::builder()
                .connect_timeout(Duration::from_secs(10))
                .timeout(Duration::from_secs(10))
                .build()
                .context("could not build HTTP client")?;
            // document.body.outlines.iter().flat_map(|opml::Outline {})
            let feed_urls = outlines
                .into_iter()
                .flat_map(|opml::Outline { xml_url, .. }| xml_url)
                .map(|feed_url| {
                    let client = client.clone();
                    async move {
                        let body = client
                            .get(&feed_url)
                            .send()
                            .await
                            .with_context(|| format!("could not fetch URL '{feed_url}'"))?
                            .bytes()
                            .await
                            .with_context(|| format!("could not read body of URL '{feed_url}'"))?;
                        // let (channel_id, channel, items) = trass::parse_feed(&feed_url, &body)
                        //     .context("could not parse RSS/atom feed")?;

                        anyhow::Ok((feed_url, body))
                    }
                });
            let mut feeds = futures::stream::iter(feed_urls).buffer_unordered(32);
            let mut n_feeds = 0;
            while let Some(res) = feeds.next().await {
                let res = res.and_then(|(feed_url, body)| {
                    eprintln!("Importing '{feed_url}'...");
                    db.try_update(|model| model.hydrate_channel(&feed_url, &body))
                        .context("could not import channel")?;
                    Ok(())
                });
                match res {
                    Ok(()) => {}
                    Err(err) => {
                        eprintln!("{err:#?}");
                        continue;
                    }
                }
                n_feeds += 1;
            }
            changed = true;
            println!("Successfully imported {n_feeds} channels.");
        }
    }

    if changed {
        db.save(std::fs::File::create(doc_path).context("could not open CRDT file")?)
            .context("could not save CRDT to file")?;
    }

    Ok(())
}

// fn parse_edit(buf: &str) -> Result<Vec<(Url, Vec<(Url, Item)>)>, nom::error::Error<&str>> {
//     use nom::bytes::complete::take_till1;
//     use nom::character::complete::{char, multispace0, one_of, space0};
//     use nom::combinator::{all_consuming, map, map_res};
//     use nom::multi::many0;
//     use nom::sequence::{delimited, preceded, terminated, tuple};
//     use nom::Finish;
//     let channel_url = preceded(
//         tuple((char('#'), space0)),
//         map_res(take_till1(|c| c == '\n'), |s: &str| Url::parse(s.trim())),
//     );
//     let item = map(
//         preceded(
//             tuple((char('-'), space0)),
//             tuple((
//                 delimited(
//                     char('['),
//                     map(one_of(" x"), |c: char| Item { read: c == 'x' }),
//                     char(']'),
//                 ),
//                 map_res(take_till1(|c| c == '\n'), |s: &str| Url::parse(s.trim())),
//             )),
//         ),
//         |(item, url)| (url, item),
//     );
//     let channel = tuple((channel_url, many0(preceded(multispace0, item))));
//     all_consuming(terminated(
//         many0(preceded(multispace0, channel)),
//         multispace0,
//     ))(buf)
//     .finish()
//     .map(|(_, output)| output)
// }

// async fn pull_channel(url: Url) -> anyhow::Result<(Url, Vec<Url>)> {
//     let bytes = reqwest::get(url.clone())
//         .await
//         .context("could not fetch RSS")?
//         .bytes()
//         .await
//         .context("could not fetch RSS")?;
//     let channel = rss::Channel::read_from(bytes.as_ref()).context("could not parse RSS")?;
//     Ok((
//         url,
//         channel
//             .items
//             .into_iter()
//             .filter_map(|item| item.link)
//             .filter_map(|url| Url::parse(&url).ok())
//             .collect(),
//     ))
// }

fn channel_id_for_pattern(model: &Model, pattern: String) -> anyhow::Result<&str> {
    if let Ok(index) = pattern.parse::<usize>() {
        model
            .channels
            .keys()
            .nth(index - 1)
            .map(|s| s.as_str())
            .context("index out of bound")
    } else {
        model
            .channels
            .keys()
            .find(|id| id.matches(&pattern).next().is_some())
            .map(|s| s.as_str())
            .context("could not find matching channel")
    }
}

#[derive(Debug, Parser)]
#[command(name = "trass")]
#[command(about = "A local-first RSS tracker.")]
struct Cli {
    #[arg(short, long, env = "TRASS_SERVER_URL")]
    server_url: Url,
    #[arg(short, long, env = "TRASS_DOCUMENT_ID")]
    document_id: Uuid,
    #[command(subcommand)]
    command: Option<Command>,
}

#[derive(Debug, Default, Subcommand)]
enum Command {
    /// List available channels.
    #[default]
    List,
    /// List items from a channel.
    Show {
        pattern: String,
    },
    /// Pull items from channels.
    Pull,
    /// Mark an item as read or unread.
    Mark {
        pattern: String,
        index: usize,
        /// Mark as unread instead of read.
        #[arg(short, long, default_value_t = false)]
        unread: bool,
    },
    /// Add a channel.
    Add {
        feed_url: String,
    },
    /// Remove a channel.
    Remove {
        pattern: String,
    },
    /// Synchronize with other devices.
    Sync,

    Edit,
    Import {
        path: PathBuf,
    },
}
